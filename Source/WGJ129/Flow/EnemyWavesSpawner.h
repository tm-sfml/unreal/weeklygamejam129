// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "EnemyWavesSpawner.generated.h"

USTRUCT(BlueprintType)
struct FShrineWaveData
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    AActor* SpawnPoint;
    UPROPERTY(EditAnywhere)
    AActor* MoveMarker;
    UPROPERTY(EditAnywhere)
    AActor* ShrineFightAreaMarker;
    UPROPERTY(EditAnywhere)
    float StartTimestamp;
    UPROPERTY(EditAnywhere)
    int Count;
};

USTRUCT()
struct FDeserterWaveData
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    AActor* SpawnPoint;
    UPROPERTY(EditAnywhere)
    AActor* MoveMarker;
    UPROPERTY(EditAnywhere)
    float StartTimestamp;
    UPROPERTY(EditAnywhere)
    int ChaserCount;
    UPROPERTY(EditAnywhere)
    int DeserterCount;
};


USTRUCT()
struct FWaveSpawnSettings
{
    GENERATED_BODY()

    UPROPERTY(EditAnywhere)
    TSubclassOf<APawn> EnemyCreepClass;
    UPROPERTY(EditAnywhere)
    TSubclassOf<APawn> ChaserCreepClass;
    UPROPERTY(EditAnywhere)
    TSubclassOf<APawn> DeserterCreepClass;
    UPROPERTY(EditAnywhere)
    float EnemyWaveRadius;
};

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WGJ129_API UEnemyWavesSpawner : public UActorComponent
{
    GENERATED_BODY()

public:
    // Sets default values for this component's properties
    UEnemyWavesSpawner();

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

public:
    // Called every frame
    virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    UPROPERTY(EditAnywhere, Category = WaveSpawn, meta = (AllowPrivateAccess = "true"))
    FWaveSpawnSettings Settings;

    UPROPERTY(EditAnywhere, Category = WaveSpawn, meta = (AllowPrivateAccess = "true"))
    TArray<FShrineWaveData> ShrineWaves;

    int CurrentShrineWaveIndex;

    void SpawnShrineWave(FShrineWaveData const& waveData) const;
    static TArray<FVector> GetSpawnPositionsNear(FVector center, int count, float radius);
    void DebugMarkSpawnPosition(TArray<FVector> const& positions) const;
};
