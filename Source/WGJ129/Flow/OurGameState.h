#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "OurGameState.generated.h"

// want to save static bool "IsDebugShown", but don't know where to? think no more!
struct FFightAreaDebug
{
    bool Ally;
    bool Enemy;
};

struct DebugData
{
    TMap<AActor*, FFightAreaDebug> FightAreaMarkerToSphereDrawn;
};

UCLASS()
class WGJ129_API AOurGameState : public AGameStateBase
{
    GENERATED_BODY()
public:
    DebugData DebugData;
};
