#include "EnemyWavesSpawner.h"
#include "Engine/World.h"
#include "GameFramework/GameStateBase.h"
#include <Creep/CreepAIController.h>
#include <Utils/macro_shortcuts.h>
#include "DrawDebugHelpers.h"


UEnemyWavesSpawner::UEnemyWavesSpawner()
{
    PrimaryComponentTick.bCanEverTick = true;
}

void UEnemyWavesSpawner::BeginPlay()
{
    Super::BeginPlay();

    auto projection = [](FShrineWaveData const& data) {
        return data.StartTimestamp;
    };
    Algo::SortBy(ShrineWaves, projection);

    CurrentShrineWaveIndex = 0;
}


void UEnemyWavesSpawner::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
    Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    float const now = GetWorld()->GetGameState()->GetServerWorldTimeSeconds();

    if (ShrineWaves.IsValidIndex(CurrentShrineWaveIndex))
    {
        auto const& currentShrineWave = ShrineWaves[CurrentShrineWaveIndex];
        if (currentShrineWave.StartTimestamp <= now)
        {
            SpawnShrineWave(currentShrineWave);
            CurrentShrineWaveIndex++;
        }
    }
}

void UEnemyWavesSpawner::SpawnShrineWave(FShrineWaveData const& waveData) const
{
    DEBUG_LOGF("wave spawned: %d", waveData.StartTimestamp);
    if (!waveData.SpawnPoint || !waveData.MoveMarker || !waveData.ShrineFightAreaMarker)
    {
        ERROR_LOG("wave data pointers not set");
        return;
    }
    if (!Settings.EnemyCreepClass)
    {
        ERROR_LOG("EnemyCreepClass not set");
        return;
    }

    TArray<FVector> const spawnPositions = GetSpawnPositionsNear(waveData.SpawnPoint->GetActorLocation(), waveData.Count, Settings.EnemyWaveRadius);

    for (FVector const& position : spawnPositions)
    {
        FActorSpawnParameters spawnParams;
        spawnParams.Owner = GetOwner();
        spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

        APawn* enemy = GetWorld()->SpawnActorAbsolute<APawn>(Settings.EnemyCreepClass, FTransform(position), spawnParams);
        auto* creepController = Cast<ACreepAIController>(enemy->Controller);
        if (!creepController)
        {
            ERROR_LOG("creepController not found");
            return;
        }
        creepController->SetMoveMarker(waveData.MoveMarker);
        creepController->SetFightAreaMarker(waveData.ShrineFightAreaMarker);
    }
    DebugMarkSpawnPosition(spawnPositions);
}

TArray<FVector> UEnemyWavesSpawner::GetSpawnPositionsNear(FVector center, int count, float radius)
{
    TArray<FVector> res;
    for (int i = 0; i < count; i++)
    {
        FVector direction(FMath::VRand());
        direction.Z = 0.f;
        float const offset = FMath::FRandRange(0.f, radius);
        res.Add(center + (direction * offset));
    }
    return res;
}

void UEnemyWavesSpawner::DebugMarkSpawnPosition(TArray<FVector> const& positions) const
{
    FColor const color = FColor::MakeRandomColor();
    for (auto const& position : positions)
    {
        DrawDebugPoint(GetWorld(), position, 10.f, color, true, 25.f);
    }
}
