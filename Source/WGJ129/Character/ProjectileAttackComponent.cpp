#include "ProjectileAttackComponent.h"
#include "..\Character\ProjectileAttackComponent.h"
#include "GameFramework/Actor.h"
#include <Engine/World.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

void UProjectileAttackComponent::BeginPlay()
{
    Super::BeginPlay();
    ENSURE(ProjectileSpawnPoint);
    ENSURE(ProjectileClass);
}

void UProjectileAttackComponent::FireCustomProjectile(FVector2D targetLocation, TSubclassOf<AActor> projectileClass)
{
    if (!projectileClass)
    {
        ERROR_LOG("projectileClass null");
        return;
    }

    if (!ProjectileSpawnPoint)
    {
        ERROR_LOG("ProjectileSpawnPoint null");
        return;
    }

    FVector const spawnPoint = ProjectileSpawnPoint->GetComponentToWorld().GetLocation();

    FActorSpawnParameters spawnParams;
    spawnParams.Owner = GetOwner();
    spawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

    // spawn the projectile at the muzzle
    auto* projectileActor = GetOwner()->GetWorld()->SpawnActor<AActor>(projectileClass, spawnPoint, FRotator(), spawnParams);
    auto* projectile = projectileActor->FindComponentByClass<UProjectileMovementComponent>();
    projectile->bIsHomingProjectile = false;

    FVector actorCenter;
    FVector actorBoxExtent;
    GetOwner()->GetActorBounds(true, actorCenter, actorBoxExtent);

    FVector target(targetLocation, actorCenter.Z + actorBoxExtent.Z);
    FVector directionToTarget = target - spawnPoint;
    directionToTarget.Normalize();
    projectile->Velocity = directionToTarget * projectile->InitialSpeed;
}

void UProjectileAttackComponent::OnReloaded()
{
    IsReloaded = true;
}

void UProjectileAttackComponent::TryFireInDirection(FHitResult const& targetHit)
{
    if (!IsReloaded)
    {
        return;
    }

    FireCustomProjectile(FVector2D(targetHit.Location), ProjectileClass);
    IsReloaded = false;

    auto& timerManager = GetWorld()->GetTimerManager();
    timerManager.SetTimer(ReloadTimer, this, &ThisClass::OnReloaded, BetweenShotDelay, true);
}
