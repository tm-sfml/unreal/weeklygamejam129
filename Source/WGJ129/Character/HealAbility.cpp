#include "HealAbility.h"
#include "GameFramework/PlayerController.h"
#include <Character/ProjectileAttackComponent.h>
#include <Engine/World.h>
#include <GameFramework/Actor.h>


void UHealAbility::OnUse(FVector mouseHitLocation)
{
    Super::OnUse(mouseHitLocation);

    // HACK: global player(
    auto* player = GWorld->GetFirstPlayerController()->GetPawn();
    if (!player)
    {
        ERROR_LOG("player null");
        return;
    }

    auto* projectileAttack = player->FindComponentByClass<UProjectileAttackComponent>();
    if (!projectileAttack)
    {
        ERROR_LOG("projectileAttack missing");
        return;
    }

    projectileAttack->FireCustomProjectile(FVector2D(mouseHitLocation), ProjectileClass);
}
