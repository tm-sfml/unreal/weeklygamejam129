#include "AbilitiesComponent.h"
#include "Engine/World.h"
#include <Character/BurnTheHereticAbility.h>
#include <Character/HealAbility.h>


UAbilitiesComponent* UAbilitiesComponent::GetGlobalUAbilitiesComponent(UWorld* world)
{
    if (!world)
    {
        ERROR_LOG("world is null");
        return nullptr;
    }
    auto* player = world->GetFirstPlayerController()->GetPawn();
    if (!player)
    {
        ERROR_LOG("player is null");
        return nullptr;
    }

    auto* abilities = player->FindComponentByClass<UAbilitiesComponent>();
    if (!abilities)
    {
        ERROR_LOG("abilities component missing");
        return nullptr;
    }

    return abilities;
}

void UAbilitiesComponent::BeginPlay()
{
    Super::BeginPlay();
    UnlockAbility<UHealAbility>();
}

void UAbilitiesComponent::SelectAbilityForTargeting(TSubclassOf<UAbility> type)
{
    TargetingAbilityType = type;
}

void UAbilitiesComponent::UseSelectedAbility(FVector mouseHitLocation)
{
    auto* ability = *Abilities.Find(TargetingAbilityType);
    if (!ability)
    {
        ERROR_LOG("ability missing");
        return;
    }

    ability->TryUse(mouseHitLocation);
    DeselectTargetingAbility();
}
