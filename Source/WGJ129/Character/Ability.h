#pragma once
#include "CoreMinimal.h"
#include "Ability.generated.h"

// uobject purely for class rtti
UCLASS(Blueprintable, BlueprintType, EditInlineNew)
class WGJ129_API UAbility : public UObject
{
    GENERATED_BODY()
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Abilities)
    float Cooldown = 0.f;
    // 0 for ready, 1 for full cooldown
    float GetCooldownPercent() const;

    // uses if cooldow passed
    void TryUse(FVector mouseHitLocation);

protected:
    FTimerHandle CooldownTimer;
    // HACK: this is actually abstract
    virtual void OnUse(FVector mouseHitLocation) {}
};
