#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "ProjectileAttackComponent.generated.h"

class UProjectileMovementComponent;
class USceneComponent;

// range attack firing linear projectiles
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WGJ129_API UProjectileAttackComponent : public UActorComponent
{
    GENERATED_BODY()

public:
    // added to our parent, set up in blueprints
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttack, meta = (AllowPrivateAccess = "true"))
    USceneComponent* ProjectileSpawnPoint = nullptr;

    void BeginPlay() override;

    // fires a custom projectile once, using setupepd spawn point and owner
    void FireCustomProjectile(FVector2D location, TSubclassOf<AActor> projectileClass);

    // fires if cooldown has passed
    // direction is [X, Y] only, Z is direction to owners center (assuming targets are the same size as owner)
    void TryFireInDirection(FHitResult const& );

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttack, meta = (AllowPrivateAccess = "true"))
    float BetweenShotDelay = 0.f;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ProjectileAttack, meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AActor> ProjectileClass = nullptr;

    bool IsReloaded = true;
    FTimerHandle ReloadTimer;

    void OnReloaded();
};
