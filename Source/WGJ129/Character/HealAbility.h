#pragma once

#include "Character/Ability.h"
#include "CoreMinimal.h"
#include <Utils/macro_shortcuts.h>
#include "HealAbility.generated.h"

UCLASS()
class WGJ129_API UHealAbility : public UAbility
{
    GENERATED_BODY()
protected:
    void OnUse(FVector mouseHitLocation) override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Heal, meta = (AllowPrivateAccess = "true"))
    TSubclassOf<AActor> ProjectileClass = nullptr;
};
