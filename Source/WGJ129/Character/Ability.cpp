#include "Ability.h"
#include "TimerManager.h"
#include <Engine/World.h>

float UAbility::GetCooldownPercent() const
{
    auto& timerManager = GetWorld()->GetTimerManager();
    if (!timerManager.IsTimerActive(CooldownTimer))
    {
        return 0.f;
    }

    float const cooldownRemaining = timerManager.GetTimerRemaining(CooldownTimer);
    return cooldownRemaining / Cooldown;
}

void UAbility::TryUse(FVector mouseHitLocation)
{
    auto& timerManager = GetWorld()->GetTimerManager();
    bool const isOnCooldown = timerManager.IsTimerActive(CooldownTimer);
    if (isOnCooldown)
    {
        return;
    }

    OnUse(mouseHitLocation);
    timerManager.SetTimer(CooldownTimer, Cooldown, false);
}
