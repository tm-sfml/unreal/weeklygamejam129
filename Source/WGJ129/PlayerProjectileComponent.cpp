#include "PlayerProjectileComponent.h"
#include <Creep/HealthComponent.h>
#include <GameFramework/Actor.h>
#include <Utils/macro_shortcuts.h>

// Called when the game starts
void UPlayerProjectileComponent::BeginPlay()
{
    Super::BeginPlay();
    GetOwner()->OnActorBeginOverlap.AddDynamic(this, &ThisClass::OnOwnerBeginOverlap);
}

//void UPlayerProjectileComponent::OnOwnerHit(AActor* otherActor, AActor* TargetActor, FVector, const FHitResult&)
//{
//
//}

//void UPlayerProjectileComponent::OnOwnerBeginOverlap(UPrimitiveComponent*, AActor* otherActor, UPrimitiveComponent*, int32, bool, const FHitResult&)
//{
//        ENSURE(GetOwner() != otherActor);
//
//    auto const enemyTag = TEXT("EnemyCreep");
//    if (!otherActor->Tags.Contains(enemyTag))
//    {
//        return;
//    }
//
//    auto* health = otherActor->FindComponentByClass<UHealthComponent>();
//    if (!health)
//    {
//        ERROR_LOG("health missing");
//        return;
//    }
//
//    health->TakeDamage(Damage);
//
//    GetOwner()->Destroy();
//}

void UPlayerProjectileComponent::OnOwnerBeginOverlap(AActor* owner, AActor* otherActor)
{
    ENSURE(GetOwner() != otherActor);

    auto const enemyTag = TEXT("EnemyCreep");
    if (!otherActor->Tags.Contains(enemyTag))
    {
        return;
    }

    auto* health = otherActor->FindComponentByClass<UHealthComponent>();
    if (!health)
    {
        ERROR_LOG("health missing");
        return;
    }

    health->TakeDamage(Damage);

    GetOwner()->Destroy();
}
