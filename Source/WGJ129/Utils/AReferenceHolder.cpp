#include "AReferenceHolder.h"
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

AAReferenceHolder* AAReferenceHolder::GetInstance()
{
    if (!Instance)
    {
        Instance = WorldUtils::FindFirstActorInWorld<AAReferenceHolder>(GWorld);
        ENSURE_MSG(Instance, "reference holder not found");
    }
    return Instance;
}

void AAReferenceHolder::PreInitializeComponents()
{
    Super::PreInitializeComponents();
    Instance = nullptr;
}
