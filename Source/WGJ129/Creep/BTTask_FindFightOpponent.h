#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "CoreMinimal.h"
#include <BehaviorTree/BTService.h>
#include "BTTask_FindFightOpponent.generated.h"

// finds the opponent from fight area to attack
UCLASS()
class WGJ129_API UBTTask_FindFightOpponent : public UBTService
{
    GENERATED_BODY()
public:
    virtual void TickNode(UBehaviorTreeComponent& ownerComp, uint8* nodeMemory, float dt) override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FBlackboardKeySelector FightOpponentKey;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    float FightAreaRadius;

    void SetFightOpponentInBlackboard(ACharacter* opponent, UBehaviorTreeComponent& ownerComp);
};
