#include "MyBTTask_SetBlackboardValue.h"
#include <BehaviorTree/BlackboardComponent.h>

EBTNodeResult::Type UMyBTTask_SetBlackboardValue::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8*)
{
    ownerComp.GetBlackboardComponent()->SetValueAsBool(BlackboardKey.SelectedKeyName, Value);
    return EBTNodeResult::Succeeded;
}
