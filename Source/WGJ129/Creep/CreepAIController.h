// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "AIController.h"
#include "BehaviorTree/BehaviorTreeTypes.h"
#include "CoreMinimal.h"
#include "CreepAIController.generated.h"

/**
 * 
 */
UCLASS()
class WGJ129_API ACreepAIController : public AAIController
{
    GENERATED_BODY()
public:
    void BeginPlay() override;
    void OnPossess(APawn* pawn) override;

    // in behavior tree
    AActor* GetFightAreaMarker() const;
    void SetMoveMarker(AActor* moveMarker);
    void SetFightAreaMarker(AActor* fightAreaMarker);
    // those that attack us
    int GetFightOpponentsCount() const;
    // increment count
    void IncrementOpponentsCount();

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FName MoveMarkerBlackboardKey;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FName FightAreaMarkerBlackboardKey;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FName FightOpponentsCountBlackboardKey = L"FightOpponentsCount";

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    UBehaviorTree* BehaviorTree = nullptr;
};
