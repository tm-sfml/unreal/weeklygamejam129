#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "MyBTTask_SetBlackboardValue.generated.h"

UCLASS()
class WGJ129_API UMyBTTask_SetBlackboardValue : public UBTTask_BlackboardBase
{
	GENERATED_BODY()
public:
    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    bool Value = false;
};
