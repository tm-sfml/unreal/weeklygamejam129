#include "MeleeAttackComponent.h"
#include "BehaviorTree/Decorators/BTDecorator_Loop.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Actor.h"
#include "HealthComponent.h"
#include <Engine/World.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include <Math/UnrealMathUtility.h>
#include <Utils/macro_shortcuts.h>
#include <Utils/utils.h>

namespace
{
float const OutOfRangeCheckInterval = 0.3f;
}

// Sets default values for this component's properties
UMeleeAttackComponent::UMeleeAttackComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
}

bool UMeleeAttackComponent::IsInRange(AActor* target) const
{
    float const ourRadius = WorldUtils::GetRadius(GetOwner());
    float const targetRadius = WorldUtils::GetRadius(target);
    
    float const distance = FVector::DistXY(target->GetActorLocation(), GetOwner()->GetActorLocation());
    return (distance - ourRadius - targetRadius) <= (Range + AllowedRangeError);
}

void UMeleeAttackComponent::Attack(AActor* target, TFunction<void()> onAttackEnd, TFunction<void()> onSingleAttack)
{
    if (!ENSURE(target))
    {
        return;
    }

    OnAttackEnd = onAttackEnd;
    OnSingleAttack = onSingleAttack;
    Target = target;

    auto& timerManager = GetWorld()->GetTimerManager();
    timerManager.SetTimer(ReloadTimer, this, &ThisClass::OnReloaded, BetweenAttackDelay, true);
}

void UMeleeAttackComponent::StopAttack()
{
    DEBUG_LOG_FUNC()
    if (ENSURE(OnAttackEnd))
    {
        OnAttackEnd();
    }
    Target = nullptr;

    auto& timerManager = GetWorld()->GetTimerManager();
    timerManager.ClearTimer(ReloadTimer);
}

void UMeleeAttackComponent::OnReloaded()
{
    if (!ENSURE(Target))
    {
        return;
    }

    if (!IsInRange(Target))
    {
        StopAttack();
        return;
    }

    DEBUG_LOG(TEXT("attack"));
    auto* health = Target->FindComponentByClass<UHealthComponent>();
    if (!ENSURE(health))
    {
        return;
    }

    bool const targetKilled = health->TakeDamage(Damage);
    if (OnSingleAttack)
    {
        OnSingleAttack();
    }

    if (targetKilled)
    {
        StopAttack();
    }
}

float UMeleeAttackComponent::GetCapsuleRadius(AActor* actor)
{
    auto* capsule = actor->FindComponentByClass<UCapsuleComponent>();
    if (!capsule)
    {
        ERROR_LOG("capsule not found");
        return 0.f;
    }

    return capsule->GetScaledCapsuleRadius();
}
