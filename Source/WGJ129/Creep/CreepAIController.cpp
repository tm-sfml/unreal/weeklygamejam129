#include "CreepAIController.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Int.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"
#include <Creep/CreepBehavior.h>
#include <Creep/HealthComponent.h>
#include <Utils/macro_shortcuts.h>

void ACreepAIController::BeginPlay()
{
    Super::BeginPlay();


}

void ACreepAIController::OnPossess(APawn* pawn)
{
    Super::OnPossess(pawn);

    if (!ENSURE(BehaviorTree))
    {
        return;
    }

    RunBehaviorTree(BehaviorTree);

    auto* health = GetPawn()->FindComponentByClass<UHealthComponent>();
    if (!ENSURE(health))
    {
        return;
    }

    auto die = [this] {
        GetPawn()->Destroy();
    };
    health->OnDeath.AddLambda(die);

    auto* creepData = GetPawn()->FindComponentByClass<UCreepBehavior>();
    if (!ENSURE(creepData))
    {
        return;
    }
    SetMoveMarker(creepData->MoveMarker);
    SetFightAreaMarker(creepData->FightAreaMarker);
}

AActor* ACreepAIController::GetFightAreaMarker() const
{
    return Cast<AActor>(GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(FightAreaMarkerBlackboardKey));
}

void ACreepAIController::SetMoveMarker(AActor* moveMarker)
{
    GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(MoveMarkerBlackboardKey, moveMarker);
}

void ACreepAIController::SetFightAreaMarker(AActor* fightAreaMarker)
{
    GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(FightAreaMarkerBlackboardKey, fightAreaMarker);
}

int ACreepAIController::GetFightOpponentsCount() const
{
    return GetBlackboardComponent()->GetValue<UBlackboardKeyType_Int>(FightOpponentsCountBlackboardKey);
}

void ACreepAIController::IncrementOpponentsCount()
{
    int const currentOpponentsCount = GetFightOpponentsCount();
    GetBlackboardComponent()->SetValue<UBlackboardKeyType_Int>(FightOpponentsCountBlackboardKey, currentOpponentsCount + 1);
}
