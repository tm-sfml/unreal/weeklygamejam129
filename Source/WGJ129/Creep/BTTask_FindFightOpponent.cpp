#include "BTTask_FindFightOpponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "DrawDebugHelpers.h"
#include <Algo/AnyOf.h>
#include <Algo/Transform.h>
#include <Creep/CreepAIController.h>
#include <Creep/CreepBehavior.h>
#include <Engine/World.h>
#include <Flow/OurGameState.h>
#include <GameFramework/Actor.h>
#include <GameFramework/Character.h>
#include <Public\GenericPlatform\GenericPlatformMath.h>
#include <Utils/macro_shortcuts.h>

void UBTTask_FindFightOpponent::TickNode(UBehaviorTreeComponent& ownerComp, uint8* memory, float dt)
{
    auto* currentOpponent = ownerComp.GetBlackboardComponent()->GetValueAsObject(FightOpponentKey.SelectedKeyName);
    if (currentOpponent)
    {
        return;
    }

    // get all in area
    auto* creepController = Cast<ACreepAIController>(ownerComp.GetAIOwner());
    FVector areaCenter = creepController->GetFightAreaMarker()->GetActorLocation();

    TArray<FHitResult> overlaps;
    FCollisionShape const fightArea = FCollisionShape::MakeSphere(FightAreaRadius);

    GetWorld()->SweepMultiByChannel(overlaps, areaCenter, areaCenter, FQuat::Identity, ECC_WorldDynamic, fightArea);
    bool isAlly = creepController->GetPawn()->ActorHasTag(L"Creep");
    auto& debugData = Cast<AOurGameState>(GetWorld()->GetGameState())->DebugData.FightAreaMarkerToSphereDrawn;
    auto& t = debugData.FindOrAdd(creepController->GetFightAreaMarker());
    bool haveDrawnDebug = isAlly ? t.Ally : t.Enemy;
    if (!haveDrawnDebug)
    {
        FColor color = isAlly ? FColor::Green : FColor::Red;
        DrawDebugSphere(GetWorld(), areaCenter, FightAreaRadius, 100, color, true, 10000);
        DrawDebugPoint(GetWorld(), areaCenter, 10.f, color, true, 25.f);

        if (isAlly)
        {
            t.Ally = true;
        }
        else
        {
            t.Enemy = true;
        }
    }

    auto* creepAIData = creepController->GetPawn()->FindComponentByClass<UCreepBehavior>();
    if (!creepAIData)
    {
        ERROR_LOG("creepAIData missing");
        return;
    }

    TArray<AActor*> enemies;
    auto isEnemy = [creepAIData](FHitResult const& overlap) {
        auto isEnemyTag = [creepAIData](FName const& tag) {
            return creepAIData->EnemyActorTags.Contains(tag);
        };
        bool const isEnemy = overlap.GetActor()->Tags.ContainsByPredicate(isEnemyTag);
        return isEnemy;
    };
    Algo::TransformIf(overlaps, enemies, isEnemy, [](FHitResult const& overlap) { return overlap.GetActor(); });

    if (enemies.Num() == 0)
    {
        // all dead, as is only right and proper
        SetFightOpponentInBlackboard(nullptr, ownerComp);
        return;
    }

    auto getFightOpponents = [creepAIData](AActor* enemy) {
        auto* creepController = Cast<ACreepAIController>(Cast<APawn>(enemy)->Controller);
        ENSURE(creepController);
        return creepController->GetFightOpponentsCount();
    };

    //pick the most unenthusiastic guy - who fights the least amount of enemies
    Algo::SortBy(enemies, getFightOpponents);

    auto* chosenOpponent = enemies[0];
    SetFightOpponentInBlackboard(Cast<ACharacter>(chosenOpponent), ownerComp);
    auto* opponentController = Cast<ACreepAIController>(Cast<APawn>(chosenOpponent)->Controller);
    opponentController->IncrementOpponentsCount();

    return;
}

void UBTTask_FindFightOpponent::SetFightOpponentInBlackboard(ACharacter* opponent, UBehaviorTreeComponent& ownerComp)
{
    ownerComp.GetBlackboardComponent()->SetValueAsObject(FightOpponentKey.SelectedKeyName, opponent);
}
