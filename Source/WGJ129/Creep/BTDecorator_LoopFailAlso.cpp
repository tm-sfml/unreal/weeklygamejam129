#include "BTDecorator_LoopFailAlso.h"
#include "BehaviorTree/BTCompositeNode.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTDecorator_LoopFailAlso::UBTDecorator_LoopFailAlso()
    : Super()
{
    bNotifyDeactivation = true;
}

void UBTDecorator_LoopFailAlso::OnNodeDeactivation(FBehaviorTreeSearchData& SearchData, EBTNodeResult::Type NodeResult)
{
    Super::OnNodeDeactivation(SearchData, NodeResult);

    if (IsInfinite)
    {
        GetParentNode()->SetChildOverride(SearchData, ChildIndex);
        return;
    }

    //auto& blackboard = *SearchData.OwnerComp.GetBlackboardComponent();
    //ExitConditionKey.ResolveSelectedKey(*blackboard.GetBlackboardAsset());
    //bool const keyPresent = blackboard.IsValidKey(ExitConditionKey.GetSelectedKeyID());

    bool const shouldLoop = EvaluateOnBlackboard(*SearchData.OwnerComp.GetBlackboardComponent());
    //bool const shouldContinue = isExitValueValid == IsInversed();
    if (shouldLoop)
    {
        GetParentNode()->SetChildOverride(SearchData, ChildIndex);
    }
}
