#include "MyBTTask_FindClosestShrine.h"
#include "BehaviorTree/BlackboardComponent.h"
#include <AIController.h>
#include <Runtime/Engine/Classes/GameFramework/Actor.h>
#include <Utils/utils.h>

EBTNodeResult::Type UMyBTTask_FindClosestShrine::ExecuteTask(UBehaviorTreeComponent& ownerComp, uint8*)
{
    auto isShrine = [](AActor const& actor) {
        return actor.ActorHasTag(L"Shrine");
    };

    auto* closestShrine = WorldUtils::FindClosetActor<AActor>(GetWorld(), isShrine, ownerComp.GetAIOwner()->GetPawn());

    if (!closestShrine)
    {
        return EBTNodeResult::Failed;
    }

    ownerComp.GetBlackboardComponent()->SetValueAsObject(ClosestShrineKey.SelectedKeyName, closestShrine);
    return EBTNodeResult::Succeeded;
}
