#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

struct Resist
{
    float Percentage = 0.f;
    FTimerHandle DurationTimer;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class WGJ129_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
    DECLARE_EVENT(UHealthComponent, FDeathEvent)
    FDeathEvent OnDeath;

	// Sets default values for this component's properties
	UHealthComponent();

    void BeginPlay() override;

    UFUNCTION(BlueprintCallable, Category = Health)
    float GetHealthPercentage() const;

    bool TakeDamage(int amount);
    //bool IsAlive() const { return  }
    //void AddDamageResist(DamageType type, float duration);
    //// forever
    //void AddDamageResist(DamageType type);

private:
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Health, meta=(AllowPrivateAccess = "true"))
    int MaxHealth = 0;

    UPROPERTY(EditAnywhere, Category = Health, meta=(AllowPrivateAccess = "true"))
    bool DestroyOwnerOnDeath = false;

    UPROPERTY(BlueprintReadOnly, Category = Health, meta=(AllowPrivateAccess = "true"))
    int Health = 0;

    //TMap<DamageType, Resist> DamageResists;
};
