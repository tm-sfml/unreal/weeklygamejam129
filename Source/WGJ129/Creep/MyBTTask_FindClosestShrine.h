#pragma once

#include "BehaviorTree/BTTaskNode.h"
#include "CoreMinimal.h"
#include "MyBTTask_FindClosestShrine.generated.h"

UCLASS()
class WGJ129_API UMyBTTask_FindClosestShrine : public UBTTaskNode
{
    GENERATED_BODY()
public:
    EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    FBlackboardKeySelector ClosestShrineKey;
};
