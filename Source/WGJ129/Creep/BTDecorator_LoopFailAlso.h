// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "BehaviorTree/Decorators/BTDecorator_Blackboard.h"
#include "CoreMinimal.h"
#include "BTDecorator_LoopFailAlso.generated.h"

/**
 * 
 */
UCLASS()
class WGJ129_API UBTDecorator_LoopFailAlso : public UBTDecorator_Blackboard
{
    GENERATED_BODY()
public:
    UBTDecorator_LoopFailAlso();

protected:
    virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override { return true; }

    void OnNodeDeactivation(FBehaviorTreeSearchData& SearchData, EBTNodeResult::Type NodeResult) override;

private:
    //UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    //FBlackboardKeySelector ExitConditionKey;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = CreepAI, meta = (AllowPrivateAccess = "true"))
    bool IsInfinite = false;
};
