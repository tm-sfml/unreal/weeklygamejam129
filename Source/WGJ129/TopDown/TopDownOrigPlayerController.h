// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include <Utils/macro_shortcuts.h>
#include "TopDownOrigPlayerController.generated.h"

class ATopDownOrigCharacter;


UCLASS()
class ATopDownOrigPlayerController : public APlayerController
{
    GENERATED_BODY()

public:
    ATopDownOrigPlayerController();

private:
    /** A decal that projects to the cursor location. */
    class AActor* CursorToWorld = nullptr;

    void BeginPlay() override;

    /** True if the controlled character should navigate to the mouse cursor. */
    uint32 bMoveToMouseCursor : 1;

    void PlayerTick(float dt) override;
    void SetupInputComponent() override;

    /** Navigate player to the current mouse cursor location. */
    void MoveToMouseCursor();

    void OnSetDestinationPressed();
    void OnSetDestinationReleased();
    void OnFirePressed();

    // templates are needed to maintain void() signature for event binding
    template <class TAbility>
    void OnAbilityKeyPressed()
    {
        auto* abilities = GetPawn()->FindComponentByClass<UAbilitiesComponent>();
        if (!abilities)
        {
            ERROR_LOG("abilities component missing");
            return;
        }

        if (abilities->IsAbilitySelected())
        {
            abilities->DeselectTargetingAbility();
        }
        else
        {
            abilities->SelectAbilityForTargeting(TAbility::StaticClass());
        }
    }

    ATopDownOrigCharacter* GetCastedCharacter() const;
};
