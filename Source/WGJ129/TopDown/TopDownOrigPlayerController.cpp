#include "TopDownOrigPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Engine/World.h"
#include "Materials/Material.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "Runtime/Engine/Classes/Components/StaticMeshComponent.h"
#include "Runtime/Engine/Classes/Engine/StaticMesh.h"
#include "UObject/ConstructorHelpers.h"
#include <Character/AbilitiesComponent.h>
#include <Character/BurnTheHereticAbility.h>
#include <Character/HealAbility.h>
#include <Character/ProjectileAttackComponent.h>
#include <TopDown/TopDownOrigCharacter.h>
#include <Utils/AReferenceHolder.h>

ATopDownOrigPlayerController::ATopDownOrigPlayerController()
{
    bShowMouseCursor = true;
    DefaultMouseCursor = EMouseCursor::Crosshairs;
    bHidden = false;
}

void ATopDownOrigPlayerController::BeginPlay()
{
    Super::BeginPlay();

    if (!AAReferenceHolder::GetInstance())
    {
        return;
    }
    CursorToWorld = AAReferenceHolder::GetInstance()->GetReference<AActor>(WorldActorReference::CursorDecal);
}

void ATopDownOrigPlayerController::PlayerTick(float dt)
{
    Super::PlayerTick(dt);

    // keep updating the destination every tick while desired
    if (bMoveToMouseCursor)
    {
        MoveToMouseCursor();

        if (!CursorToWorld)
        {
            return;
        }

        FHitResult traceHitResult;
        GetHitResultUnderCursor(ECC_Visibility, true, traceHitResult);
        FVector const cursorFv = traceHitResult.ImpactNormal;
        FRotator const cursorR = cursorFv.Rotation();
        CursorToWorld->SetActorLocation(traceHitResult.Location);
        CursorToWorld->SetActorRotation(cursorR);
    }
}

void ATopDownOrigPlayerController::SetupInputComponent()
{
    // set up gameplay key bindings
    Super::SetupInputComponent();

    InputComponent->BindAction("SetDestination", IE_Pressed, this, &ThisClass::OnSetDestinationPressed);
    InputComponent->BindAction("SetDestination", IE_Released, this, &ThisClass::OnSetDestinationReleased);
    InputComponent->BindAction("Fire", IE_Pressed, this, &ThisClass::OnFirePressed);

    InputComponent->BindAction("1", IE_Pressed, this, &ThisClass::OnAbilityKeyPressed<UHealAbility>);
    InputComponent->BindAction("2", IE_Pressed, this, &ThisClass::OnAbilityKeyPressed<UHealAbility>);
    InputComponent->BindAction("3", IE_Pressed, this, &ThisClass::OnAbilityKeyPressed<UBurnTheHereticAbility>);
}

void ATopDownOrigPlayerController::MoveToMouseCursor()
{
    FHitResult Hit;
    GetHitResultUnderCursor(ECC_Visibility, false, Hit);
    if (!Hit.bBlockingHit)
    {
        return;
    }
    if (GetCastedCharacter())
    {
        GetCastedCharacter()->SetNewMoveDestination(Hit.ImpactPoint);
    }
}


void ATopDownOrigPlayerController::OnSetDestinationPressed()
{
    // set flag to keep updating destination until released
    bMoveToMouseCursor = true;
}

void ATopDownOrigPlayerController::OnSetDestinationReleased()
{
    // clear flag to indicate we should stop updating the destination
    bMoveToMouseCursor = false;
}

void ATopDownOrigPlayerController::OnFirePressed()
{
    FHitResult hit;
    GetHitResultUnderCursor(ECC_Visibility, false, hit);
    if (!hit.bBlockingHit)
    {
        return;
    }

    auto* abilities = GetPawn()->FindComponentByClass<UAbilitiesComponent>();
    if (!abilities)
    {
        ERROR_LOG("abilities component missing");
        return;
    }

    if (abilities->IsAbilitySelected())
    {
        abilities->UseSelectedAbility(hit.Location);
    }
    else
    {
        auto* projectileAttack = GetPawn()->FindComponentByClass<UProjectileAttackComponent>();
        if (!projectileAttack)
        {
            ERROR_LOG("projectileAttack not found");
            return;
        }
        projectileAttack->TryFireInDirection(hit);
    }
}

ATopDownOrigCharacter* ATopDownOrigPlayerController::GetCastedCharacter() const
{
    return static_cast<ATopDownOrigCharacter*>(GetPawn());
}
