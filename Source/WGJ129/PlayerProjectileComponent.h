// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "CoreMinimal.h"
#include "PlayerProjectileComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class WGJ129_API UPlayerProjectileComponent : public UActorComponent
{
    GENERATED_BODY()

protected:
    // Called when the game starts
    virtual void BeginPlay() override;

private:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Projectile, meta = (AllowPrivateAccess = "true"))
    int Damage = 0;

    //UFUNCTION()
    //void OnOwnerBeginOverlap(UPrimitiveComponent* OverlappedComponent,
    //                         AActor* OtherActor,
    //                         UPrimitiveComponent* OtherComp,
    //                         int32 OtherBodyIndex,
    //                         bool bFromSweep,
    //                         const FHitResult& SweepResult);

    UFUNCTION()
    void OnOwnerBeginOverlap(AActor* owner, AActor* otherActor);
};
